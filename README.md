# 文件分享服务
####这是一个 python 写的轻量级的文件共享服务器
（基于内置的SimpleHTTPServer模块）

#####支持文件上传下载，只要你安装了python（建议版本2.6~2.7，不支持3.x)

#####1.去到想要共享的目录下，执行：
    python SimpleHTTPServerWithUpload.py 1234
```其中1234为你指定的端口号，如不写，默认为 8080
然后访问 http://localhost:1234 即可，localhost 或者 1234 请酌情替换。
此项目由：9527 贡献代码```